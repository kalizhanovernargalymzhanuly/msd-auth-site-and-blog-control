package kz.msd.site.oauth.service;

import kz.msd.site.oauth.dto.BlogDTO;
import kz.msd.site.oauth.entity.Blog;
import kz.msd.site.oauth.repository.BlogRepository;
import kz.msd.site.oauth.utils.ConflictException;
import kz.msd.site.oauth.utils.TextUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class BlogService {

    private BlogRepository blogRepository;

    @Autowired
    public BlogService(BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    public Map<String, Object> create(BlogDTO dto) {
        Map<String, Object> response = new HashMap<>();
        if (TextUtils.isBlank(dto.getTitle(), dto.getDesc(), dto.getText())) {
            throw new ConflictException("Пожалуйста, заполните все поля!");
        }
        Blog.BlogBuilder blogBuilder = Blog.builder();
        blogBuilder.setTitle(dto.getTitle());
        blogBuilder.setDesc(dto.getDesc());
        blogBuilder.setText(dto.getText());
        blogBuilder.setFile(dto.getFile());
        blogBuilder.setCreatedAt(new Date());

        Blog blog = blogBuilder.build();

        blogRepository.save(blog);
        response.put("Status", 200);
        return response;
    }

    public Map<String, Object> update(BlogDTO dto) {
        Map<String, Object> response = new HashMap<>();
        Optional<Blog> blog = blogRepository.findById(dto.getId());
        if (TextUtils.isBlank(dto.getTitle(), dto.getDesc(), dto.getText())) {
            throw new ConflictException("Пожалуйста, заполните все поля!");
        }
        if (!blog.isPresent()) {
            throw new ConflictException("Такого блога нету в базе данных!");
        }
        blog.get().setUpdatedAt(new Date());
        blog.get().setTitle(dto.getTitle());
        blog.get().setDesc(dto.getFile());
        blog.get().setText(dto.getText());
        blog.get().setFile(dto.getFile());

        blogRepository.save(blog.get());
        response.put("blog", blogRepository.findById(dto.getId()));
        response.put("Status", 200);
        return response;
    }

    public Map<String, Object> deleteById(Blog blog) {
        Map<String, Object> response = new HashMap<>();
        blogRepository.delete(blog);
        response.put("Status", 200);
        return response;
    }

    public Map<String, Object> read(BlogDTO dto) {
        Map<String, Object> response = new HashMap<>();
        Optional<Blog> optional = blogRepository.findById(dto.getId());
        if (!optional.isPresent()) {
            throw new ConflictException("Такого блога нету в базе данных!");
        }

        response.put("title", optional.get().getTitle());
        response.put("desc", optional.get().getDesc());
        response.put("text", optional.get().getText());
        response.put("file", optional.get().getFile());
        response.put("Status", 200);
        return response;
    }

    public Map<String, Object> readAll() {
        Map<String, Object> response = new HashMap<>();
        response.put("blogs", blogRepository.findAll());
        response.put("Status", 200);
        return response;
    }
}
