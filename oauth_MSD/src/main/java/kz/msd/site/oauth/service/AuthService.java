package kz.msd.site.oauth.service;

import kz.msd.site.oauth.dto.SigInDTO;
import kz.msd.site.oauth.entity.SignIn;
import kz.msd.site.oauth.repository.AdminRepository;
import kz.msd.site.oauth.utils.ConflictException;
import kz.msd.site.oauth.utils.TextUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthService {
    private AdminRepository adminRepository;

    @Autowired
    public AuthService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Map<String, Object> signIn(SigInDTO dto) {
        Map<String, Object> response = new HashMap<>();
        Optional<SignIn> signInOptional = adminRepository.findByUserNameAndPassword(dto.getUserName(), dto.getPassword());
        if (TextUtils.isBlank(dto.getUserName()) && TextUtils.isBlank(dto.getPassword())) {
            throw new ConflictException("Пожалуйста, введите данные корректно!");
        }
        if (!signInOptional.isPresent()) {
            throw new ConflictException("Неверный Логин или Пароль!");
        }
        response.put("user", dto);
        response.put("status", 200);
        return response;
    }
}
