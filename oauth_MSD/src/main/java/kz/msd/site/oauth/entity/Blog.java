package kz.msd.site.oauth.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "blog")
public class Blog {

    private Integer id;
    private String title;
    private String desc;
    private String text;
    private String file;
    private Date createdAt;
    private Date updatedAt;

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "blog_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @Column(name = "blog_title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Column(name = "blog_description")
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    @Column(name = "blog_text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    @Column(name = "blog_file")
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    @Column(name = "created_at")
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    @Column(name = "updated_at")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public static BlogBuilder builder(){
        return new BlogBuilder();
    }

    public static class BlogBuilder {
        private String title;
        private String desc;
        private String text;
        private String file;
        private Date createdAt;
        private Date updatedAt;

        public void setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
        }

        public void setUpdatedAt(Date updatedAt) {
            this.updatedAt = updatedAt;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public Blog build() {
            Blog blog = new Blog();
            blog.desc = desc;
            blog.title = title;
            blog.text = text;
            blog.file = file;
            blog.createdAt = createdAt;
            blog.updatedAt = updatedAt;
            return blog;
        }
    }
}
