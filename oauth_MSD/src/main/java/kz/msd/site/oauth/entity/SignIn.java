package kz.msd.site.oauth.entity;

import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "admin")
public class SignIn {
    private Integer id;
    private String userName;
    private String password;

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @NotNull
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @NotNull
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static SignInBuilder builder(){
        return new SignInBuilder();
    }

    public static class SignInBuilder {
        private Integer id;
        private String userName;
        private String password;

        public void setId(Integer id) {
            this.id = id;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public SignIn build() {
            SignIn signIn = new SignIn();

            signIn.password = password;
            signIn.userName = userName;
            return signIn;
        }
    }

}
