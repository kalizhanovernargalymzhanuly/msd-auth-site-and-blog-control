package kz.msd.site.oauth.controller;

import kz.msd.site.oauth.dto.BlogDTO;
import kz.msd.site.oauth.dto.SigInDTO;
import kz.msd.site.oauth.entity.Blog;
import kz.msd.site.oauth.service.AuthService;

import kz.msd.site.oauth.service.BlogService;
import kz.msd.site.oauth.utils.ConflictException;
import kz.msd.site.oauth.utils.ExceptionResponseBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class AdminController {

    @Autowired
    AuthService authService;

    @Autowired
    BlogService blogService;

    @PostMapping("auth/admin")
    ResponseEntity<Map<String, Object>> signIn(@RequestBody SigInDTO dto) {
        try {
            Map<String, Object> response = authService.signIn(dto);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch ( ConflictException ext) {
            return new ResponseEntity<>(ExceptionResponseBase.create(ext.getMessage(), ext.getStatus()), HttpStatus.valueOf(ext.getStatus()));
        } catch (Exception ext) {
            return new ResponseEntity<>(ExceptionResponseBase.create(ext.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("createBlog")
    ResponseEntity<Map<String, Object>> createBlog(@RequestBody BlogDTO dto) {
        try {
            Map<String, Object> response = blogService.create(dto);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (ConflictException ex) {
            return new ResponseEntity<>(ExceptionResponseBase.create(ex.getMessage(), ex.getStatus()), HttpStatus.valueOf(ex.getStatus()));
        }catch (Exception exc) {
            return new ResponseEntity<>(ExceptionResponseBase.create(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("editBlog")
    public ResponseEntity<Map<String, Object>> updateBlog(@RequestBody BlogDTO dto) {
        try {
            Map<String, Object> response = blogService.update(dto);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (ConflictException ex){
            return new ResponseEntity<>(ExceptionResponseBase.create(ex.getMessage(), ex.getStatus()), HttpStatus.valueOf(ex.getStatus()));
        }catch (Exception exc) {
            return new ResponseEntity<>(ExceptionResponseBase.create(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("deleteBlog")
    ResponseEntity<Map<String, Object>> deleteBlog(@RequestBody Blog blog) {
        try {
            Map<String, Object> response = blogService.deleteById(blog);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (ConflictException ex){
            return new ResponseEntity<>(ExceptionResponseBase.create(ex.getMessage(), ex.getStatus()), HttpStatus.valueOf(ex.getStatus()));
        }catch (Exception exc) {
            return new ResponseEntity<>(ExceptionResponseBase.create(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("readBlog")
    ResponseEntity<Map<String, Object>> readBlog(@RequestBody BlogDTO dto) {
        try {
            Map<String, Object> response = blogService.read(dto);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (ConflictException ex) {
            return new ResponseEntity<>(ExceptionResponseBase.create(ex.getMessage(), ex.getStatus()), HttpStatus.valueOf(ex.getStatus()));
        }catch (Exception exc) {
            return new ResponseEntity<>(ExceptionResponseBase.create(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("readAllBlog")
    ResponseEntity<Map<String, Object>> readAllBlog() {
        try {
            Map<String, Object> response = blogService.readAll();
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (ConflictException ex) {
            return new ResponseEntity<>(ExceptionResponseBase.create(ex.getMessage(), ex.getStatus()), HttpStatus.valueOf(ex.getStatus()));
        }catch (Exception exc) {
            return new ResponseEntity<>(ExceptionResponseBase.create(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.FORBIDDEN);
        }
    }
}
