package kz.msd.site.oauth.utils;

import org.apache.commons.lang3.StringUtils;

public class TextUtils {
    public static boolean isBlank(String firstText){
        return StringUtils.isBlank(firstText);
    }

    public static boolean isBlank(String firstText, String secondText){
        return StringUtils.isBlank(firstText) || StringUtils.isBlank(secondText);
    }
    public static boolean isBlank(String firstText, String secondText, String thirdText){
        return StringUtils.isBlank(firstText) || StringUtils.isBlank(secondText) || StringUtils.isBlank(thirdText);
    }
    public static boolean isBlank(String firstText, String secondText, String thirdText, String fourthText){
        return StringUtils.isBlank(firstText) || StringUtils.isBlank(secondText) || StringUtils.isBlank(thirdText) ||
                StringUtils.isBlank(fourthText);
    }
    public static boolean isBlank(String firstText, String secondText, String thirdText,
                                  String fourthText, String fifthText){
        return StringUtils.isBlank(firstText) || StringUtils.isBlank(secondText) || StringUtils.isBlank(thirdText) ||
                StringUtils.isBlank(fourthText) || StringUtils.isBlank(fifthText);
    }
    public static boolean isBlank(String firstText, String secondText, String thirdText,
                                  String fourthText, String fifthText, String sixthText){

        return StringUtils.isBlank(firstText) || StringUtils.isBlank(secondText) || StringUtils.isBlank(thirdText) ||
                StringUtils.isBlank(fourthText) || StringUtils.isBlank(fifthText) || StringUtils.isBlank(sixthText);
    }
}
