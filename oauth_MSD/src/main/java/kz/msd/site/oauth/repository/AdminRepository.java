package kz.msd.site.oauth.repository;

import kz.msd.site.oauth.entity.SignIn;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<SignIn, Long>, JpaSpecificationExecutor<SignIn> {
    Optional<SignIn> findByUserNameAndPassword(String userName, String password);
}
